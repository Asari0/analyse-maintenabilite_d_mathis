import { RentalCalculationStrategy } from './rentalCalculationStrategies';

export class Car {
    private _title: string;
    private _rentalCalculationStrategy: RentalCalculationStrategy;

    constructor(title: string, rentalCalculationStrategy: RentalCalculationStrategy) {
        this._title = title;
        this._rentalCalculationStrategy = rentalCalculationStrategy;
    }

    // Methode pour obtenir le titre de la voiture
    getTitle(): string {
        return this._title;
    }

    // Methode pour calculer le montant de la location
    calculateAmount(daysRented: number): number {
        return this._rentalCalculationStrategy.calculateAmount(daysRented);
    }
}
