import { Rental } from './rental';

export class Customer {
    private _name: string;
    private _rentals: Rental[] = [];

    constructor(name: string) {
        this._name = name;
    }

    // Methode pour ajouter une location
    addRental(arg: Rental): void {
        this._rentals.push(arg);
    }

    // Methode pour obtenir le nom du client
    getName(): string {
        return this._name;
    }

    // Methode pour calculer les points de fidelite
    private calculateFrequentRenterPoints(): number {
        let frequentRenterPoints = 0;

        for (const rental of this._rentals) {
            const rentalAmount = rental.calculateAmount();
            const pointsFromAmount = Math.floor(rentalAmount / 1000);
            frequentRenterPoints += pointsFromAmount;
        }

        return frequentRenterPoints;
    }

    // Methode pour obtenir le montant total
    private getTotalAmount(): number {
        let totalAmount = 0;

        for (const rental of this._rentals) {
            totalAmount += rental.calculateAmount();
        }

        return totalAmount;
    }

    // Methode pour obtenir la facture
    invoice(): string {
        let result = `Rental Record for ${this.getName()}\n`;
        let totalAmount = this.getTotalAmount();
        let frequentRenterPoints = this.calculateFrequentRenterPoints();

        for (const rental of this._rentals) {
            const thisAmount = rental.calculateAmount();
            result += `\t${rental.getCar().getTitle()}\t${(thisAmount / 100).toFixed(1)}\n`;
        }

        result += `Amount owed is ${(totalAmount / 100).toFixed(1)}\n`;
        result += `You earned ${frequentRenterPoints} frequent renter points\n`;

        return result;
    }

    // Methode pour obtenir la facture en format JSON
    invoiceJson(): string {
        const rentalsDetails = this._rentals.map(rental => ({
            title: rental.getCar().getTitle(),
            amount: (rental.calculateAmount() / 100).toFixed(1)
        }));

        const totalAmount = this.getTotalAmount();
        const frequentRenterPoints = this.calculateFrequentRenterPoints();

        const invoiceDetails = {
            customerName: this.getName(),
            rentals: rentalsDetails,
            totalAmount: (totalAmount / 100).toFixed(1),
            frequentRenterPoints: frequentRenterPoints
        };

        return JSON.stringify(invoiceDetails, null, 2);
    }
}
