import { Car } from './car';

export class Rental {
    private _car: Car;
    private _daysRented: number;

    constructor(car: Car, daysRented: number) {
        this._car = car;
        this._daysRented = daysRented;
    }

    // Methode pour obtenir le nombre de jours de location
    getDaysRented(): number {
        return this._daysRented;
    }

    // Methode pour obtenir l'instance de la voiture
    getCar(): Car {
        return this._car;
    }

    // Methode pour calculer le montant de la location
    calculateAmount(): number {
        return this._car.calculateAmount(this._daysRented);
    }
}
