export interface RentalCalculationStrategy {
    calculateAmount(daysRented: number): number;
}

export class RegularCarStrategy implements RentalCalculationStrategy {

    // Methode pour calculer le montant de la location d'une voiture ordinaire
    calculateAmount(daysRented: number): number {
        let amount = 5000 + daysRented * 9500;
        if (daysRented > 5) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}

export class NewModelCarStrategy implements RentalCalculationStrategy {

    // Methode pour calculer le montant de la location d'une voiture neuve
    calculateAmount(daysRented: number): number {
        let amount = 9000 + daysRented * 15000;
        if (daysRented > 3) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}
