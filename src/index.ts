import { Car } from './classes/car';
import { Rental } from './classes/rental';
import { Customer } from './classes/customer';
import { RegularCarStrategy, NewModelCarStrategy } from './classes/rentalCalculationStrategies';

const regularCarStrategy = new RegularCarStrategy();
const newModelCarStrategy = new NewModelCarStrategy();

const car1 = new Car("Car 1", regularCarStrategy);
const car2 = new Car("Car 2", newModelCarStrategy);

const rental1 = new Rental(car1, 3);
const rental2 = new Rental(car2, 2);

const customer = new Customer("John Doe");
customer.addRental(rental1);
customer.addRental(rental2);

console.log('Invoice (Text):');
console.log(customer.invoice());

console.log('Invoice (JSON):');
console.log(customer.invoiceJson());

export { Car, Rental, Customer, RegularCarStrategy, NewModelCarStrategy };
