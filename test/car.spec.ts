import { Car } from '../src/classes/car';
import { RegularCarStrategy, NewModelCarStrategy } from '../src/classes/rentalCalculationStrategies';

describe('Car class', () => {

    test('calculateAmount should return correct amount for RegularCarStrategy', () => {
        const regularCarStrategy = new RegularCarStrategy();
        const car = new Car('Regular Car', regularCarStrategy);

        expect(car.calculateAmount(1)).toBe(14500);
        expect(car.calculateAmount(5)).toBe(52500);
        expect(car.calculateAmount(6)).toBe(22000);
    });


    test('calculateAmount should return correct amount for NewModelCarStrategy', () => {
        const newModelCarStrategy = new NewModelCarStrategy();
        const car = new Car('New Model Car', newModelCarStrategy);

        expect(car.calculateAmount(1)).toBe(24000);
        expect(car.calculateAmount(3)).toBe(54000);
        expect(car.calculateAmount(4)).toBe(49000);
    });

});
