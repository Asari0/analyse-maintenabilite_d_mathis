import { Car, Rental, Customer, RegularCarStrategy, NewModelCarStrategy } from '../src/index';

test('should correctly calculate the invoice', () => {

    const regularCarStrategy = new RegularCarStrategy();
    const newModelCarStrategy = new NewModelCarStrategy();

    const car1 = new Car("Car 1", regularCarStrategy);
    const car2 = new Car("Car 2", newModelCarStrategy);

    const rental1 = new Rental(car1, 3);
    const rental2 = new Rental(car2, 2);

    const customer = new Customer("John Doe");
    customer.addRental(rental1);
    customer.addRental(rental2);

    const rental1Amount = rental1.calculateAmount();
    const rental2Amount = rental2.calculateAmount();

    const totalAmount = rental1Amount + rental2Amount;
    const frequentRenterPoints = Math.floor(totalAmount / 1000);

    const expectedInvoice = `Rental Record for John Doe\n\t${car1.getTitle()}\t${(rental1Amount / 100).toFixed(1)}\n\t${car2.getTitle()}\t${(rental2Amount / 100).toFixed(1)}\nAmount owed is ${(totalAmount / 100).toFixed(1)}\nYou earned ${frequentRenterPoints} frequent renter points\n`;

    expect(customer.invoice()).toBe(expectedInvoice);
});



test('should correctly generate JSON invoice', () => {

    const regularCarStrategy = new RegularCarStrategy();
    const newModelCarStrategy = new NewModelCarStrategy();

    const car1 = new Car("Car 1", regularCarStrategy);
    const car2 = new Car("Car 2", newModelCarStrategy);

    const rental1 = new Rental(car1, 3);
    const rental2 = new Rental(car2, 2);

    const customer = new Customer("John Doe");
    customer.addRental(rental1);
    customer.addRental(rental2);

    const rental1Amount = rental1.calculateAmount();
    const rental2Amount = rental2.calculateAmount();

    const totalAmount = rental1Amount + rental2Amount;
    const frequentRenterPoints = Math.floor(totalAmount / 1000);

    const expectedJson = {
        customerName: "John Doe",
        rentals: [
            { title: car1.getTitle(), amount: (rental1Amount / 100).toFixed(1) },
            { title: car2.getTitle(), amount: (rental2Amount / 100).toFixed(1) }
        ],
        totalAmount: (totalAmount / 100).toFixed(1),
        frequentRenterPoints: frequentRenterPoints
    };

    expect(JSON.parse(customer.invoiceJson())).toEqual(expectedJson);
});


