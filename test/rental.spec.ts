import { Rental } from '../src/classes/rental';
import { Car } from '../src/classes/car';
import { RegularCarStrategy, NewModelCarStrategy } from '../src/classes/rentalCalculationStrategies';

describe('Rental class', () => {

    test('getDaysRented should return the correct number of days rented', () => {
        const car = new Car('Test Car', new RegularCarStrategy());
        const rental = new Rental(car, 5);
        expect(rental.getDaysRented()).toBe(5);
    });

    test('getCar should return the correct car instance', () => {
        const car = new Car('Test Car', new RegularCarStrategy());
        const rental = new Rental(car, 5);
        expect(rental.getCar()).toBe(car);
    });

    test('calculateAmount should return correct amount for RegularCarStrategy', () => {
        const car = new Car('Regular Car', new RegularCarStrategy());
        const rental = new Rental(car, 5);
        expect(rental.calculateAmount()).toBe(52500);

        const rentalLongTerm = new Rental(car, 6);
        expect(rentalLongTerm.calculateAmount()).toBe(22000);
    });

    test('calculateAmount should return correct amount for NewModelCarStrategy', () => {
        const car = new Car('New Model Car', new NewModelCarStrategy());
        const rental = new Rental(car, 3);
        expect(rental.calculateAmount()).toBe(54000);

        const rentalLongTerm = new Rental(car, 4);
        expect(rentalLongTerm.calculateAmount()).toBe(49000);
    });

});
