# Analyse de la maintenabilité d'un projet par Mathis DORE

## Introduction
Evaluation pour le cours d'analyse et mainteabilité. Projet initialement réalisé en Php puis en TypeScript pour ma part.
## Table des matières

- [Exercice 1](#exercice-1)
  - [Question 1: Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?](#question-1)
  - [Question 2: Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?](#question-2)
  - [Question 3: Comment comprenez-vous chaque étape de l’heuristique “First make it run, next make it correct, and only after that worry about making it fast” ?](#question-3)
  - [Question 4: Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?](#question-4)
- [Exercice 2](#exercice-2)
    - [Installer et lancer le projet](#installer-et-lancer-le-projet)
    - [Execution des tests](#execution-des-tests)

## Exercice 1

### Question 1: Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?

Les principales sources de complexité dans un système logiciel sont les suivantes :

- Complexité du domaine : Nature spécifique du domaine, exigences et contraintes.

- Complexité technique : Architecture, technologies et outils, couplage et dépendances, algorithmes et structures de données.

- Complexité du code : Taille, qualité du code, usage des patterns de conception.

- Complexité organisationnelle : Équipes distribuées, gestion des versions, conformité réglementaire.

- Complexité évolutive : Maintenance, évolutivité, endettement technique.

- Complexité liée à l'intégration : Interopérabilité, gestion des API et interfaces.

- Complexité utilisateur : Conception des interfaces, personnalisation et configurations.


### Question 2: Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?

Exemple de code pour illustrer les avantages de la programmation orientée interface en TypeScript :

Interface et implémentation de base :

```typescript

// Définition de l'interface
interface PaymentProcessor {
    processPayment(amount: number): void;
}

// Implémentation
class PayPalProcessor implements PaymentProcessor {
    processPayment(amount: number): void {
        console.log(`Processing payment through PayPal: ${amount}`);
    }
}

```

Utilisation de l'interface :

```typescript

class PaymentService {
    private paymentProcessor: PaymentProcessor;

    constructor(paymentProcessor: PaymentProcessor) {
        this.paymentProcessor = paymentProcessor;
    }

    makePayment(amount: number): void {
        this.paymentProcessor.processPayment(amount);
    }
}

// Exemple d'utilisation
const paypalProcessor: PaymentProcessor = new PayPalProcessor();

const paymentServiceWithPayPal = new PaymentService(paypalProcessor);

paymentServiceWithPayPal.makePayment(100);
```
Programmer vers une interface plutôt que vers une implémentation présente plusieurs avantages :

- Flexibilité et extensibilité : Permet de changer facilement l'implémentation sans modifier le code client. Dans l'exemple proposer ci-dessus on peut facilement changer l'implémentation de `PaymentProcessor` sans modifier le code de `PaymentService`.

- Réutilisabilité : Facilite la réutilisation de code avec différentes implémentations. Le meme service de paiement peut utiliser différentes implémentations de processus de paiement.

- Testabilité : Simplifie les tests unitaires en permettant de remplacer les implémentations réelles par des mocks ou des stubs. Lors des tests, vous pouvez injecter un mock de `PaymentProcessor` pour vérifier le comportement de `PaymentService` sans effectuer de réels paiements.

- Séparation des préoccupations : Encourage la séparation des responsabilités en définissant clairement ce que le code fait sans préciser comment. Le service de paiement ne se préoccupe pas de la manière dont les paiements sont traités, il utilise simplement l'interface `PaymentProcessor`.

Globalement, la programmation orientée interface favorise une conception modulaire, flexible et testable, ce qui contribue à la qualité et à la maintenabilité du code.


### Question 3: Comment comprenez-vous chaque étape de l’heuristique “First make it run, next make it correct, and only after that worry about making it fast” ?

L'heuristique de développement de système proposée par Eric Steven Raymond se décompose en trois étapes essentielles : "D'abord, faites en sorte que ça fonctionne, ensuite assurez-vous que ce soit correct, et seulement après, préoccupez-vous de le rendre rapide." Voici comment chaque étape peut être comprise et appliquée dans le contexte du développement logiciel :

#### Faites en sorte que ça fonctionne

Compréhension : Cette première étape consiste à créer une version fonctionnelle du logiciel, même si elle est rudimentaire ou incomplète. L'objectif est d'obtenir un prototype ou une version initiale qui réalise les fonctionnalités de base. Il s'agit d'une approche pragmatique pour valider que le concept et la structure de base du logiciel sont viables.

Application :

- Prototypage rapide : Créez une version minimale viable (MVP) qui permet de tester et démontrer les fonctionnalités principales.

- Priorisation des fonctionnalités : Concentrez-vous sur les fonctionnalités essentielles qui répondent aux besoins critiques des utilisateurs.

- Feedback rapide : Utilisez cette version pour obtenir des retours d'expérience précoces des utilisateurs ou des parties prenantes, afin d'orienter les développements futurs.

#### Assurez-vous que ce soit correct

Compréhension : Une fois que le logiciel fonctionne, il est crucial de s'assurer qu'il fonctionne correctement. Cette étape implique de vérifier que le logiciel répond aux spécifications, qu'il est fiable et qu'il produit des résultats exacts. Cela inclut la correction des bugs, l'amélioration de la robustesse et la validation de la logique métier.

Application :

- Tests approfondis : Effectuez des tests unitaires, d'intégration, fonctionnels et de régression pour valider que le logiciel fonctionne comme prévu.

- Refactorisation : Améliorez et nettoyez le code pour le rendre plus compréhensible et maintenable, sans ajouter de nouvelles fonctionnalités.

- Validation métier : Vérifiez que le logiciel répond aux besoins des utilisateurs et aux exigences définies.

#### Préoccupez-vous de le rendre rapide

Compréhension : La performance devient une préoccupation seulement après que le logiciel fonctionne correctement. À cette étape, il s'agit d'optimiser le logiciel pour qu'il soit plus rapide et plus efficace. Les optimisations prématurées peuvent compliquer inutilement le développement et introduire des bugs, c'est pourquoi elles sont laissées pour la fin.

Application :

- Profilage et analyse des performances : Identifiez les goulots d'étranglement et les parties du code qui nécessitent des optimisations.

- Optimisations ciblées : Appliquez des améliorations de performance là où elles sont réellement nécessaires, sans compromettre la lisibilité ou la maintenabilité du code.

- Tests de performance : Effectuez des tests de charge et de stress pour assurer que le logiciel peut supporter les conditions d'utilisation prévues.
Compréhension de l'heuristique dans son ensemble

- Cette heuristique fournit une approche pragmatique et progressive pour le développement logiciel. Elle reconnaît que :

- Fonctionnalité de base : La première priorité est de s'assurer que le logiciel accomplit la tâche pour laquelle il est destiné. Si le logiciel ne fonctionne pas, les autres aspects sont sans importance.

- Correction et robustesse : Une fois la fonctionnalité de base établie, il est essentiel de garantir que le logiciel est correct et fiable. Un logiciel qui fonctionne mais produit des résultats incorrects est inutile.

- Optimisation de la performance : Les améliorations de performance sont importantes, mais seulement après avoir établi que le logiciel fonctionne correctement. Cela permet d'éviter les optimisations prématurées qui peuvent compliquer le développement et introduire des erreurs.

En résumé, cette heuristique encourage une approche itérative et centrée sur les priorités réelles du développement logiciel, favorisant la fonctionnalité et la fiabilité avant de s'attaquer à l'optimisation des performances.


### Question 4: Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?

Pour mener à bien un travail de refactoring, voici les méthodes et approches recommandées :

- Tests Automatisés : Écrivez et exécutez des tests unitaires et d'intégration pour assurer que le comportement reste inchangé.

- Refactoring Graduel : Faites des changements par petites étapes et commettez fréquemment.

- Techniques de Refactoring : Utilisez des techniques comme le renommage, l'extraction de méthodes, et la suppression de code mort.

- Révision de Code : Faites des révisions de code par les pairs pour identifier des problèmes potentiels.

- Documentation : Documentez les changements et ajoutez des commentaires explicatifs dans le code.

- Outils de Refactoring : Utilisez les fonctionnalités de refactoring des IDEs et des outils d'analyse statique.

- Définir des Objectifs Clairs : Identifiez les parties du code à refactorer et définissez des objectifs clairs.

- Refactoring Continu : Intégrez le refactoring dans le processus de développement continu et encouragez une culture de refactoring.

Ces approches assurent un refactoring efficace et minimisent les risques de nouveaux bugs.



## Exercice 2

### Installer et lancer le projet

```bash
# Cloner le dépôt
$ git clone https://gitlab.com/Asari0/analyse-maintenabilite_d_mathis.git

# Installer les dépendances
$ npm install

# Build le projet
$ npm run build

# Lancer le projet
$ npm start
```

### Execution des tests

```bash
# Lancer les tests
$ npm test

ou 

$ npm run test
```