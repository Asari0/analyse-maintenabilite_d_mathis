import { RentalCalculationStrategy, RegularCarStrategy, NewModelCarStrategy } from '../src/classes/rentalCalculationStrategies';

describe('RegularCarStrategy', () => {

    test('calculateAmount should return correct amount for RegularCarStrategy', () => {
        const strategy: RentalCalculationStrategy = new RegularCarStrategy();

        expect(strategy.calculateAmount(1)).toBe(14500);
        expect(strategy.calculateAmount(5)).toBe(52500);
        expect(strategy.calculateAmount(6)).toBe(22000);
    });
});


describe('NewModelCarStrategy', () => {

    test('calculateAmount should return correct amount for NewModelCarStrategy', () => {
        const strategy: RentalCalculationStrategy = new NewModelCarStrategy();

        expect(strategy.calculateAmount(1)).toBe(24000);
        expect(strategy.calculateAmount(3)).toBe(54000);
        expect(strategy.calculateAmount(4)).toBe(49000);
    });
});
